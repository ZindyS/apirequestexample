package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    val TAG = "ERRROR"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .baseUrl("http://95.31.130.149:8085/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(CodemaniaApi::class.java)

        api.getProfileCourses(6).enqueue(object : Callback<List<Course>> {
            override fun onResponse(call: Call<List<Course>>, response: Response<List<Course>>) {
                if (response.isSuccessful) {
                    findViewById<RecyclerView>(R.id.recyclerView).adapter = RecyclerCourseAdapter(response.body()!!)
                } else {
                    Log.d(TAG, response.message())
                }
            }

            override fun onFailure(call: Call<List<Course>>, t: Throwable) {
                Log.d(TAG, t.message!!)
            }
        })
    }
}