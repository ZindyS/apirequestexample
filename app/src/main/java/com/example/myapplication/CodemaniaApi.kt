package com.example.myapplication

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CodemaniaApi {
    @GET("profile/courses")
    fun getProfileCourses(@Header("idUser") id : Int) : Call<List<Course>>
    //Если рядом с запросом есть замочек, то пишем @Header("idUser")
    //Если в запросе прописано Тело запроса, то пишем @Body
    //Если в запросе прописан параметр и под ним написан query, то пишем @Query(""), внутри кавычек прописываем название параметра
}