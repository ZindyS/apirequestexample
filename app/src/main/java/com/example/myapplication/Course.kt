package com.example.myapplication

data class Course(
    val id : Int,
    val title : String,
    val tags : List<Int>,
    val cover : String,
    val price : Int
)
